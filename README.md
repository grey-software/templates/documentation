# Documentation

Template for Grey Software documentation websites

[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/grey-software/templates/documentation)

## TODO:

Add GitLab and linkedin icons to the NAV bar
Outline which fields to change when using the template
Link relevant documentation
